--- Initialization Script
local AddOnName, nsVars = ...

--Cache global variables
local CreateFrame = CreateFrame

-- Prepare Addon variables
local SS = CreateFrame("Frame", "SSpheres_frame_addon")

SS.numFrames = 1

function SS.CreateFrame(...)
  SS.numFrames = SS.numFrames + 1
  return CreateFrame(...)
end

nsVars.SS = SS

SS.dVars = {
  spheres = {},
  global = {},
}

SS.pVars = {
  spheres = {},
}

SS._f = "SS_frame_"

SS.version = "v-1.0.11-classic"

-- Set a flag for classic/retail client.
local buildInfo = GetBuildInfo()
buildInfo = string.sub(buildInfo, 1, 2)
SS.retailClient = tonumber(buildInfo) >= 2

--- Global function for importing remote assets.
rawset(_G, "SeamoreSpheresRemoteAssets", {
  newAssetCollection = function(collectionName, collection)
    -- TODO: Add validation to ensure proper values.
    SS.FillTextures.remoteAssets[collectionName] = collection
  end,
  newSwirlAssets = function(collectionName, collection)
    -- TODO: Add validation to ensure proper values.
    SS.FillTextures.textures.swirl[collectionName] = collection
  end,
})

-- Initialize event listeners
local eventFrame = CreateFrame("Frame", SS._f .. "event_watcher")
eventFrame:RegisterEvent("ADDON_LOADED")

--- Registers an event with the event frame.
--- @param e string The event to register.
function SS:AddEvent(e)
  eventFrame:RegisterEvent(e)
end

--- Unregisters an event from the event frame.
--- @param e string The event to register.
function SS:RemoveEvent(e)
  eventFrame:UnregisterEvent(e)
end

eventFrame:HookScript("OnEvent", function(_, event, ...)
  if SS[event] then
    SS[event](SS, ...)
  end
end)
