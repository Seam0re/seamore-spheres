--- Commmand Related Functionality
--
local AddOnName, nsVars = ...
local ss = nsVars.SS

local xmlButton

--- Event handler for addon commands. ("/ss")
-- @param msg The text entered after "/ss".
--
local function debugCommand(rest)
end

--- Event handler for addon commands. ("/ss")
-- @param msg The text entered after "/ss".
--
local function CommandFunc(msg)
  local cmd, rest = msg:match("^(%S*)%s*(.-)$")

  if cmd == "nuke" then
    ss.Utils:ResetSpheres()
  elseif cmd == "debug" then
    debugCommand(rest)
  elseif cmd == "r" then
    ReloadUI()
  else
    ss.Config:Toggle()
  end
end

--setup the slash command stuff, hide the GUI
SlashCmdList["SS"] = CommandFunc
rawset(_G, "SLASH_SS1", "/ss")
