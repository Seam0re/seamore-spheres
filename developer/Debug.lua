--- Table Debugger
--
local AddOnName, nsVars = ...
local ss = nsVars.SS

local mtDebug = {
  precision = 1,
  tDesc = function(t,p,pad)
    for k,v in pairs(t) do
      local key_output = tonumber(k) or "\""..tostring(k).."\""
      if (type(v) == "table") and (p > 0) then
        print(pad.."["..key_output.."] = {")
        ss.Debug.tDesc(v,p-1,pad.."  ")
      else
        print(pad.."["..key_output.."] = "..tostring(v)..",")
      end
    end

    -- format closing braces
    pad = string.sub(pad, 3)
    local closingTag = (string.len(pad) > 0) and "}," or "};"
    print(pad..closingTag)
  end,
  describe = function(t,p)
    print("---------------------------------------------")
    print("----------------- DESCRIBE ------------------")
    print("--------------------------------------------\n")

    p = p or ss.Debug.getPrecision()

    if type(t) == "table" then
      print("table = {")
      ss.Debug.tDesc(t,p,"  ")
    else
      print(t)
    end

    print("\n---------------------------------------------")
    print("--------------- END DESCRIBE ----------------")
    print("---------------------------------------------")
  end,
  setPrecision = function(p)
    ss.Debug.precision = p
  end,
  getPrecision = function()
    return ss.Debug.precision
  end,
}

ss.Utils.addModule("Debug", mtDebug)
