--- Resource Information for Micro Spheres
--
local ClassResourceData = {}
local AddOnName, nsVars = ...
local ss = nsVars.SS

local UnitPower, UnitPowerMax, strlower = UnitPower, UnitPowerMax, strlower

--- Provides player resource information.
--- @param spell string The resource identifier.
--- @return string, number, number The current resource values.
--- @see UnitPower().
local function getMinMax(spell)
  local min, max = UnitPower("player", spell), UnitPowerMax("player", spell)
  return spell, min, max
end

ClassResourceData.classColors = {
  ["deathknight"] = {
    { "runic power", "runes", },
    { r = 0.77, g = 0.12, b = 0.23, a = 1, },
  },
  ["demonhunter"] = {
    { "fury", "pain", },
    { r = 0.64, g = 0.19, b = 0.79, a = 1, },
  },
  ["druid"] = {
    { "mana", "rage", "energy", "combo points", "lunar power", },
    { r = 1.00, g = 0.49, b = 0.04, a = 1, },
  },
  ["hunter"] = {
    { "focus", },
    { r = 0.67, g = 0.83, b = 0.45, a = 1, },
  },
  ["mage"] = {
    { "mana", "arcane charges", },
    { r = 0.41, g = 0.80, b = 0.94, a = 1, },
  },
  ["monk"] = {
    { "mana", "energy", "chi", },
    { r = 0.00, g = 1.00, b = 0.59, a = 1, },
  },
  ["paladin"] = {
    { "mana", "holy power", },
    { r = 0.96, g = 0.55, b = 0.73, a = 1, },
  },
  ["priest"] = {
    { "mana", "insanity", },
    { r = 1.00, g = 1.00, b = 1.00, a = 1, },
  },
  ["rogue"] = {
    { "energy", "combo points", },
    { r = 1.00, g = 0.96, b = 0.41, a = 1, },
  },
  ["shaman"] = {
    { "mana", "maelstrom", },
    { r = 0.00, g = 0.44, b = 0.87, a = 1, },
  },
  ["warlock"] = {
    { "mana", "soul shards", },
    { r = 0.58, g = 0.51, b = 0.79, a = 1, },
  },
  ["warrior"] = {
    { "rage", },
    { r = 0.78, g = 0.61, b = 0.43, a = 1, },
  },
}

ClassResourceData.resources = {
  ["mana"] = {
    0,
    { r = 0.000, g = 0.000, b = 1.000, a = 1 },
    getMinMax = getMinMax,
    name = "Mana",
  },
  ["rage"] = {
    1,
    { r = 1.000, g = 0.000, b = 0.000, a = 1 },
    getMinMax = getMinMax,
    name = "Rage",
  },
  ["focus"] = {
    2,
    { r = 1.000, g = 0.500, b = 0.250, a = 1 },
    getMinMax = getMinMax,
    name = "Focus",
  },
  ["energy"] = {
    3,
    { r = 1.000, g = 1.000, b = 0.000, a = 1 },
    getMinMax = getMinMax,
    name = "Energy",
  },
  ["combo points"] = {
    4,
    { r = 1.000, g = 0.960, b = 0.410, a = 1 },
    getMinMax = getMinMax,
    name = "Combo Points",
  },
  ["runes"] = {
    5,
    { r = 0.000, g = 0.820, b = 1.000, a = 1 },
    getMinMax = getMinMax,
    name = "Runes",
  },
  ["runic power"] = {
    6,
    { r = 0.000, g = 0.820, b = 1.000, a = 1 },
    getMinMax = getMinMax,
    name = "Runic Power",
  },
  ["soul shards"] = {
    7,
    { r = 0.500, g = 0.320, b = 0.550, a = 1 },
    getMinMax = getMinMax,
    name = "Soul Shards",
  },
  ["lunar power"] = {
    8,
    { r = 0.300, g = 0.520, b = 0.900, a = 1 },
    getMinMax = getMinMax,
    name = "Lunar Power",
  },
  ["holy power"] = {
    9,
    { r = 0.950, g = 0.900, b = 0.600, a = 1 },
    getMinMax = getMinMax,
    name = "Holy Power",
  },
  ["maelstrom"] = {
    11,
    { r = 0.000, g = 0.500, b = 1.000, a = 1 },
    getMinMax = getMinMax,
    name = "Maelstrom",
  },
  ["chi"] = {
    12,
    { r = 0.710, g = 1.000, b = 0.920, a = 1 },
    getMinMax = getMinMax,
    name = "Chi",
  },
  ["insanity"] = {
    13,
    { r = 0.400, g = 0.000, b = 0.800, a = 1 },
    getMinMax = getMinMax,
    name = "Insanity",
  },
  ["arcane charges"] = {
    16,
    { r = 0.100, g = 0.100, b = 0.980, a = 1 },
    getMinMax = getMinMax,
    name = "Arcane Charges",
  },
  ["fury"] = {
    17,
    { r = 0.788, g = 0.259, b = 0.992, a = 1 },
    getMinMax = getMinMax,
    name = "Fury",
  },
  ["pain"] = {
    18,
    { r = 1.000, g = 0.610, b = 0.000, a = 1 },
    getMinMax = getMinMax,
    name = "Pain",
  },
  ["ammo shot"] = {
    0,
    { r = 0.800, g = 0.600, b = 0.000, a = 1 },
    getMinMax = getMinMax,
    name = "Ammo Shot",
  },
  ["fuel"] = {
    0,
    { r = 0.000, g = 0.550, b = 0.500, a = 1 },
    getMinMax = getMinMax,
    name = "Fuel",
  },
  ["stagger light"] = {
    0,
    { r = 0.520, g = 1.000, b = 0.520, a = 1 },
    getMinMax = getMinMax,
    name = "Stagger Light",
  },
  ["stagger medium"] = {
    0,
    { r = 1.000, g = 0.980, b = 0.720, a = 1 },
    getMinMax = getMinMax,
    name = "Stagger Medium",
  },
  ["stagger heavy"] = {
    0,
    { r = 1.000, g = 0.420, b = 0.420, a = 1 },
    getMinMax = getMinMax,
    name = "Stagger Heavy",
  },
  ["health"] = {
    0,
    { r = 0.000, g = 1.000, b = 0.000, a = 1 },
    getMinMax = getMinMax,
    name = "Health",
  },
  ["gcd"] = {
    "gcd",
    { r = 0.482, g = 1.000, b = 0.827, a = 1 },
    getMinMax = getMinMax,
    name = "GCD",
  },
}

ClassResourceData.deathknight = {
  ["runes"] = ClassResourceData.resources["runes"],
}

ClassResourceData.paladin = {
  ["holypower"] = ClassResourceData.resources["holy power"],
}

ClassResourceData.mage = {
  ["arcanecharges"] = ClassResourceData.resources["arcane charges"],
}

ClassResourceData.druid = {
  ["combopoints"] = ClassResourceData.resources["combo points"],
}

ClassResourceData.monk = {
  ["chi"] = ClassResourceData.resources["chi"],
}

ClassResourceData.rogue = {
  ["combopoints"] = ClassResourceData.resources["combo points"],
}

ClassResourceData.warlock = {
  ["soulshards"] = ClassResourceData.resources["soul shards"],
}

if not ss.retailClient then
  ClassResourceData.resources["combo points"][1] = 14
  ClassResourceData.resources["focus"][1] = 0
end

ClassResourceData.resourceAnimations = {
  ["Holy Power"] = {
    ["posX"] = 0,
    ["posY"] = -0.3,
    ["alpha"] = 1,
    ["camDistance"] = 6,
    ["frameLevel"] = 4,
    ["id"] = "hammer of wrath",
    ["groupId"] = "sparkling",
  },
  ["Soul Shards"] = {
    ["posX"] = -0.02,
    ["posY"] = -0.15,
    ["alpha"] = 1,
    ["camDistance"] = 1.5,
    ["frameLevel"] = 4,
    ["id"] = "skull",
    ["groupId"] = "fire",
  },
  ["Combo Points"] = {
    ["posX"] = 0.1,
    ["posY"] = -1.5,
    ["alpha"] = 1,
    ["camDistance"] = 6,
    ["frameLevel"] = 4,
    ["id"] = "yellow fog",
    ["groupId"] = "fog",
  },
  ["Chi"] = {
    ["posX"] = 0.1,
    ["posY"] = -1.5,
    ["alpha"] = 1,
    ["camDistance"] = 6,
    ["frameLevel"] = 4,
    ["id"] = "turquoise fog",
    ["groupId"] = "fog",
  },
  --    ["white fog"] = {
  --      ["posX"] = 0,
  --      ["posY"] = 0,
  --      ["alpha"] = 1,
  --      ["camDistance"] = 4,
  --      ["frameLevel"] = 4,
  --      ["id"] = "white fog",
  --      ["groupId"] = "fog",
  --    },
}

--- Provides the color for a class.
--- @param class string The class identifier.
--- @return table The color.
function ClassResourceData.classColor(class)
  return ClassResourceData.classColors[class][2]
end

--- Provides the resource list of a class.
--- @param class string The class identifier.
--- @return table The resource list.
function ClassResourceData.classResources(class)
  return ClassResourceData.classColors[class][1]
end

--- Initializes a stored resource with actual values.
--- @param resource string The resource to initialize.
function ClassResourceData.init(resource)
  local index = strlower(resource.name, " ", "")

  if ClassResourceData.resources[index] then
    -- update identifier, color and getMinMax functions.
    resource[1] = ClassResourceData.resources[index][1]
    resource[2] = ClassResourceData.resources[index][2]
    resource.getMinMax = ClassResourceData.resources[index].getMinMax
  else
    resource.getMinMax = function()
      return "", 0, 0
    end
  end
end

--- Provides an animation for a resource if available.
--- @param resource string The resource to initialize.
--- @return table The animation data.
function ClassResourceData.resourceAnimation(resource)
  return ClassResourceData.resourceAnimations[resource]
end

--- Provides the color for a resource.
--- @param resource string The resource identifier.
--- @return table The color.
function ClassResourceData.resourceColor(resource)
  return ClassResourceData.resources[strlower(resource)][2]
end

--- Provides resource information.
--- @param resource string The resource identifier.
--- @return table The resource details.
function ClassResourceData.resourceInfo(resource)
  local res = ClassResourceData.resources[strlower(resource)]
  res.ssClass = "ClassResourceData"
  return res
end

ss.Utils.addModule("ClassResourceData", ClassResourceData)


--[[
--


resource animations for micro spheres
hammer of wrath
y = -0.3
x = 0
scale = 6
alpha = 1

power crystal
y = 0.13
x = 0
scale = 3
alpha = 1

blue portal 2
y = -0.05
x = 0
scale = 4.5
alpha = 1

warlock portal
y = -0.1
x = 0
scale = 4.5
alpha = 1

cthun
y = 0.9
x = 0
scale = 1.36
alpha = 1

drawf artifact
y = 0.9
x = 0
scale = 0.7 (big) 0.85 (small)
alpha = 1

arcane orb
y = 0.5
x = -0.15
scale = 2
alpha = 1

water planet
y = 0.35
x = -0.32
scale = 2.150
alpha = 1

force sphere
y = 0.01
x = 0
scale = 1.215
alpha = 1

purple circus
y = -0.1
x = 0.02
scale = 1.85
alpha = 1

red magnet (and white magnet)
y = 0
x = 0.02
scale = 2.5
alpha = 1

turquoise fog (and purple fog, yellow fog, green fog, red fog)
y = -1.5
x = 0.1
scale = 6
alpha = 1

orb fog
y = 0
x = 0
scale = 6
alpha = 1

white fog
y = 0
x = 0
scale = 4
alpha = 1

yellow spark
y = -0.1
x = 0
scale = 1.5 (small) 1 (large)
alpha = 1

strobo2
y = 0.01
x = -0.01
scale = 0.99 (large) 1.1 (small)
alpha = 1

fire
y = 0.01
x = -0.01
scale = 5.5
alpha = 1

skull
y = -0.15
x = -0.02
scale = 2.5
alpha = 1








-- others

brown chocolate
y = -1.09
x = -0.12
scale = 1.15
alpha = 1

purple chocolate
y = -1.35
x = -0.02
scale = 1.22
alpha = 1

--
--
--
-- ]
 ]]