local AddOnName, nsVars = ...
nsVars[AddOnName].localize()

CustomDropDownToggleButtonMixin = {}

function CustomDropDownToggleButtonMixin:OnLoad_Intrinsic()
  if self.RegisterForMouse then
    self:RegisterForMouse("LeftButtonDown", "LeftButtonUp")
  end
end

function CustomDropDownToggleButtonMixin:HandlesGlobalMouseEvent(buttonID, event)
  return event == "GLOBAL_MOUSE_DOWN" and buttonID == "LeftButton"
end

gexport()
