local AddOnName, nsVars = ...
nsVars[AddOnName].localize()

CustomDropDownList1 = CreateFrame("Button", "CustomDropDownList1", nil, ss.retailClient and "CustomDropDownList1Template" or "CustomDropDownList1ClassicTemplate")
CustomDropDownList1:SetID(1)

CustomDropDownList2 = CreateFrame("Button", "CustomDropDownList2", nil, ss.retailClient and "CustomDropDownList2Template" or "CustomDropDownList2ClassicTemplate")
CustomDropDownList2:SetID(2)

gexport()
