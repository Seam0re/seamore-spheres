local AddOnName, nsVars = ...
nsVars[AddOnName].localize()

-- Custom customdropdown buttons are instantiated by some external system.
-- When calling CustomDropDownMenu_AddButton that system sets info.customFrame to the instance of the frame it wants to place on the menu.
-- The customdropdown menu creates its button for the entry as it normally would, but hides all elements.  The custom frame is then anchored
-- to that button and assumes responsibility for all relevant customdropdown menu operations.
-- The hidden button will request a size that it should become from the custom frame.

CustomDropDownCustomMenuEntryMixin = {};

function CustomDropDownCustomMenuEntryMixin:GetPreferredEntryWidth()
  -- NOTE: Only width is currently supported, customdropdown menus size vertically based on how many buttons are present.
  return self:GetWidth();
end

function CustomDropDownCustomMenuEntryMixin:OnSetOwningButton()
  -- for derived objects to implement
end

function CustomDropDownCustomMenuEntryMixin:SetOwningButton(button)
  self:SetParent(button:GetParent());
  self.owningButton = button;
  self:OnSetOwningButton();
end

function CustomDropDownCustomMenuEntryMixin:GetOwningDropdown()
  return self.owningButton:GetParent();
end

function CustomDropDownCustomMenuEntryMixin:SetContextData(contextData)
  self.contextData = contextData;
end

function CustomDropDownCustomMenuEntryMixin:GetContextData()
  return self.contextData;
end

function CustomDropDownCustomMenuEntryMixin:OnEnter()
  CustomDropDownMenu_StopCounting(self:GetOwningDropdown());
end

function CustomDropDownCustomMenuEntryMixin:OnLeave()
  CustomDropDownMenu_StartCounting(self:GetOwningDropdown());
end

-- ButtonMixin from SharedXML\UIDropDownMenuTemplates.lua
CustomDropDownMenuButtonMixin = {}

function CustomDropDownMenuButtonMixin:OnEnter(...)
  ExecuteFrameScript(self:GetParent(), "OnEnter", ...)
end

function CustomDropDownMenuButtonMixin:OnLeave(...)
  ExecuteFrameScript(self:GetParent(), "OnLeave", ...)
end

function CustomDropDownMenuButtonMixin:OnMouseDown(button)
  if self:IsEnabled() then
    ToggleCustomDropDownMenu(nil, nil, self:GetParent())
    PlaySound(SOUNDKIT.IG_MAINMENU_OPTION_CHECKBOX_ON)
  end
end

gexport()
