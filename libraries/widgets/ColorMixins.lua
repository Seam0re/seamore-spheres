local AddOnName, nsVars = ...
nsVars[AddOnName].localize()

local RGBATableMixin = {}

function RGBATableMixin:GetColorTable()
  local t = { self:GetRGBA() }

  return {
    r = t[1],
    g = t[2],
    b = t[3],
    a = t[4],
  }
end

-- Grey colours.
SeamoreSpheresGrey1 = CreateFromMixins(CreateColor(0.05, 0.05, 0.05, 1), RGBATableMixin)
SeamoreSpheresGrey2 = CreateFromMixins(CreateColor(0.15, 0.15, 0.15, 1), RGBATableMixin)
SeamoreSpheresGrey3 = CreateFromMixins(CreateColor(0.2, 0.2, 0.2, 1), RGBATableMixin)
SeamoreSpheresGrey4 = CreateFromMixins(CreateColor(0.25, 0.25, 0.25, 1), RGBATableMixin)
SeamoreSpheresGrey5 = CreateFromMixins(CreateColor(0.5, 0.5, 0.5, 1), RGBATableMixin)
SeamoreSpheresGrey6 = CreateFromMixins(CreateColor(0.7, 0.7, 0.7, 1), RGBATableMixin)
SeamoreSpheresWhite = CreateFromMixins(CreateColor(1, 1, 1, 1), RGBATableMixin)

-- Blue colours.
SeamoreSpheresBlue1 = CreateFromMixins(CreateColor(0, 0.5, 1, 1), RGBATableMixin)

-- Green colours.
SeamoreSpheresGreen1 = CreateFromMixins(CreateColor(0.5, 1, 0.3, 1), RGBATableMixin)

-- Brown colours.
SeamoreSpheresBrown1 = CreateFromMixins(CreateColor(1, 0.7, 0.5, 1), RGBATableMixin)

gexport()
