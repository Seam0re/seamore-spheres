local AddOnName, nsVars = ...
nsVars[AddOnName].localize()

local LAYOUT_TYPE_CONFIRM, LAYOUT_TYPE_NOTIFICATION = 1, 2

local txtClose = ss.T["btn_close"]
local txtConfirm = ss.T["lbl_confirmation"]
local txtConfirmContent = ss.T["lbl_are_you_sure"]
local txtNo = ss.T["btn_no"]
local txtYes = ss.T["btn_yes"]

CustomNotificationDialogMixin = {}

CustomNotificationDialogMixin.LAYOUT_TYPE_CONFIRM = LAYOUT_TYPE_CONFIRM
CustomNotificationDialogMixin.LAYOUT_TYPE_NOTIFICATION = LAYOUT_TYPE_NOTIFICATION

function CustomNotificationDialogMixin:SetLayoutType(layoutType)
  if layoutType == LAYOUT_TYPE_NOTIFICATION then
    self.Title:SetText("")
    self.Content:SetText("")
    self.Button1:SetText(txtClose)
    self.Button2:Hide()
  else
    self.Title:SetText(txtConfirm)
    self.Content:SetText(txtConfirmContent)
    self.Button1:SetText(txtNo)
    self.Button2:SetText(txtYes)
    self.Button2:Show()
  end

  self.onYes = nil
  self.onNo = nil
  self.layoutType = layoutType
end

function CustomNotificationDialogMixin:OnLoad()
  self:SetLayoutType(self.LAYOUT_TYPE_CONFIRM)
end

function CustomNotificationDialogButton1_OnClick(self)
  self:GetParent():Hide()

  if self:GetParent().layoutType == LAYOUT_TYPE_CONFIRM and self:GetParent().onNo then
    self:GetParent().onNo()
  end
end

function CustomNotificationDialogButton2_OnClick(self)
  self:GetParent():Hide()

  if self:GetParent().onYes then
    self:GetParent().onYes()
  end
end

gexport()
