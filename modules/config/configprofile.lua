--- Profile Configuration Panel
--
local AddOnName, nsVars = ...
local ss = nsVars.SS

local string = string
local CustomDropDownMenu_SetSelectedID, CustomDropDownMenu_SetText = CustomDropDownMenu_SetSelectedID, CustomDropDownMenu_SetText
local CloseCustomDropDownMenus = CloseCustomDropDownMenus
local CustomDropDownMenu_SetSelectedValue = CustomDropDownMenu_SetSelectedValue
local UIDropDownMenu_GetText = ss.APIUtils.UIDropDownMenu_GetText

ss["ConfigProfile"] = {}

--- Sets default layout values.
--
local initProfileSettings = function()
  local profileStart = { string.split("_", ss.profile.profileName) };
  CustomDropDownMenu_SetText(ss.ConfigProfile.ProfileToUseDropDown, profileStart[2])
  CustomDropDownMenu_SetText(ss.ConfigProfile.CopyProfileDropDown, ss.T["menu_item_profile_select"])
  CustomDropDownMenu_SetText(ss.ConfigProfile.DeleteProfileDropDown, ss.T["menu_item_profile_select"])
  ss.ConfigProfile.newProfileEditBox:SetText("")
  ss.ConfigProfile.DeleteProfileButton:Hide()
end

--- Initializes the profile config panel.
--
function ss:InitializeConfigProfilePanel()
  -- Create the config preview frame
  ss.ConfigProfile = ss.Config:AddTab(ss._f .. "Config_Profile", ss.T["tab_label_profile"], initProfileSettings)

  ss.ConfigProfile:HookScript("OnHide", function()
    ss.Notification:CloseConfirm()
  end)

  -- Add the widget container
  ss.ConfigProfile.widgetFrame = ss.ConfigTools.getWidgetContainer(ss.ConfigProfile, "ProfileWidgetFrame")

  -- Add the title
  ss.ConfigProfile.widgetFrame.Title = ss.ConfigTools.createTitle(ss.T["config_title_profile"], ss.ConfigProfile.widgetFrame)

  local dropdownLabelMargin = 179
  local dropdownWidth = 200
  local dropdownXPos = 65

  -- Add profile select dropdown
  ss.ConfigTools.addWidget {
    type = "DropDown",
    name = "ProfileToUseDropDown",
    parent = ss.ConfigProfile,
    tAlign = "LEFT",
    width = dropdownWidth,
    height = 50,
    anchor = "TOP",
    x = dropdownXPos,
    y = (ss.ConfigTools.topMargin - (ss.ConfigTools.itemHeight * 2) - 15),
    init = function(_, level)
      for k in ss.Utils.pairsByKeys(ss.global) do
        local stuff = {
          fontObject = "Exo2Medium12",
        }

        local profileStart = { string.split("_", k) };

        if profileStart[1] == "SSProfile" then
          stuff.text = profileStart[2]
          stuff.value = k
          stuff.checked = (ss.profile.profileName == k)

          stuff.func = function(self)
            CustomDropDownMenu_SetSelectedID(ss.ConfigProfile.ProfileToUseDropDown, self:GetID())
            CustomDropDownMenu_SetText(ss.ConfigProfile.ProfileToUseDropDown, self:GetText())
            ss.profile.profileName = k
            ss.db = ss.Utils:SetMetatables(ss.global[k], ss.private["SSProfile_Default"]);
            ss.Layout:Refresh()
            ss.Layout:UpdateScale()
            CloseCustomDropDownMenus()
          end

          securecall("CustomDropDownMenu_AddButton", stuff, level)
        end
      end
    end,
    label = {
      name = "ProfileToUseLabel",
      text = ss.T["menu_label_profile_to_use"],
      leftLabelMargin = dropdownLabelMargin,
    },
  }

  -- Add profile copy dropdown
  ss.ConfigTools.addWidget {
    type = "DropDown",
    name = "CopyProfileDropDown",
    parent = ss.ConfigProfile,
    tAlign = "LEFT",
    width = dropdownWidth,
    height = 50,
    anchor = "TOP",
    x = dropdownXPos,
    y = (ss.ConfigTools.topMargin - (ss.ConfigTools.itemHeight * 3) - 15),
    init = function(_, level)
      for k in ss.Utils.pairsByKeys(ss.global) do
        local stuff = {
          fontObject = "Exo2Medium12",
        }

        local profileStart = { string.split("_", k) };

        if profileStart[1] == "SSProfile" then
          stuff.notCheckable = true
          stuff.text = profileStart[2]
          stuff.value = profileStart[2]

          stuff.func = function()
            ss.Notification:Confirm(function()
              ss.global[ss.profile.profileName] = ss.Utils.tcopy(ss.global[k], true)
              ss.db = ss.Utils:SetMetatables(ss.global[ss.profile.profileName], ss.private["SSProfile_Default"])

              ss.Layout:Refresh()
              ss.Layout:UpdateScale()
            end)

            CloseCustomDropDownMenus()
          end

          securecall("CustomDropDownMenu_AddButton", stuff, level)
        end
      end
    end,
    label = {
      name = "CopyProfileLabel",
      text = ss.T["menu_label_copy_profile"],
      leftLabelMargin = dropdownLabelMargin,
    },
  }

  -- Add the create profile field
  ss.ConfigProfile.newProfileEditBox = ss.ConfigTools.createEditBox {
    parent = ss.ConfigProfile,
    name = "NewProfileEditBox",
    width = dropdownWidth,
    height = 25,
    anch = "TOP",
    x = dropdownXPos,
    y = (ss.ConfigTools.topMargin - (ss.ConfigTools.itemHeight * 5) - 15),
    letters = 20,
    onEnterPressed = function(self)
      self:ClearFocus()
    end,
    onTextChanged = function(self)
      if self:GetText() == "" then
        ss.ConfigProfile.CreateProfileButton:Hide()
      else
        ss.ConfigProfile.CreateProfileButton:Show()
      end
    end,
    label = {
      name = "NewProfileEditBoxLabel",
      text = ss.T["btn_create_profile"],
      leftLabelMargin = dropdownLabelMargin,
    },
  }

  ss.ConfigProfile.CreateProfileButton = ss.ConfigTools.createButton {
    parent = ss.ConfigProfile,
    name = "CreateProfileButton",
    anch = "TOP",
    x = dropdownXPos,
    y = (ss.ConfigTools.topMargin - (ss.ConfigTools.itemHeight * 6) - 15),
    text = ss.T["btn_create_profile"],
    onClick = function()
      local val = string.gsub(ss.ConfigProfile.newProfileEditBox:GetText(), "_", "")

      if val ~= "" and not ss.global["SSProfile_" .. val] then
        ss.global["SSProfile_" .. val] = {}
        ss.profile.profileName = "SSProfile_" .. val
        ss.db = ss.Utils:SetMetatables(ss.global[ss.profile.profileName], ss.private["SSProfile_Default"])

        ss.ConfigProfile.newProfileEditBox:SetText("")
        CustomDropDownMenu_SetSelectedValue(ss.ConfigProfile.ProfileToUseDropDown, val)
        CustomDropDownMenu_SetText(ss.ConfigProfile.ProfileToUseDropDown, val)
        CustomDropDownMenu_SetText(ss.ConfigProfile.DeleteProfileDropDown, ss.T["btn_delete_profile"])
        ss.ConfigProfile.DeleteProfileButton:Hide()

        ss.Layout:Refresh()
        ss.Layout:UpdateScale()
      end
    end,
  }

  -- Add the delete profile field
  ss.ConfigTools.addWidget {
    type = "DropDown",
    name = "DeleteProfileDropDown",
    parent = ss.ConfigProfile,
    tAlign = "LEFT",
    width = dropdownWidth,
    height = 50,
    anchor = "TOP",
    x = dropdownXPos,
    y = (ss.ConfigTools.topMargin - (ss.ConfigTools.itemHeight * 8) - 15),
    init = function(_, level)
      for k in ss.Utils.pairsByKeys(ss.global) do
        local stuff = {
          fontObject = "Exo2Medium12",
        }

        local profileStart = { string.split("_", k) };

        if profileStart[1] == "SSProfile" then
          stuff.notCheckable = true
          stuff.text = profileStart[2]
          stuff.value = profileStart[2]

          stuff.func = function(self)
            ss.ConfigProfile.DeleteProfileButton:Show()
            CustomDropDownMenu_SetText(ss.ConfigProfile.DeleteProfileDropDown, self:GetText())
            CloseCustomDropDownMenus()
          end

          securecall("CustomDropDownMenu_AddButton", stuff, level)
        end
      end
    end,
    label = {
      name = "DeleteProfileDropDownLabel",
      text = ss.T["btn_delete_profile"],
      leftLabelMargin = dropdownLabelMargin,
    },
  }

  ss.ConfigProfile.DeleteProfileButton = ss.ConfigTools.createButton {
    parent = ss.ConfigProfile,
    name = "DeleteProfileButton",
    anch = "TOP",
    x = dropdownXPos,
    y = (ss.ConfigTools.topMargin - (ss.ConfigTools.itemHeight * 9) - 15),
    text = ss.T["btn_delete_profile"],
    onClick = function()
      local val = UIDropDownMenu_GetText(ss.ConfigProfile.DeleteProfileDropDown)

      ss.Notification:Confirm(function()
        if val ~= "" and val ~= ss.T["btn_delete_profile"] and val ~= "Default" then
          local refresh

          if ss.profile.profileName == "SSProfile_" .. val then
            ss.profile.profileName = "SSProfile_Default"
            ss.db = ss.Utils:SetMetatables(ss.global["SSProfile_Default"], ss.private["SSProfile_Default"]);
            refresh = true
          end

          ss.global["SSProfile_" .. val] = nil

          ss.ConfigProfile.DeleteProfileButton:Hide()
          CustomDropDownMenu_SetText(ss.ConfigProfile.DeleteProfileDropDown, ss.T["btn_delete_profile"])

          if refresh then
            ss.Layout:Refresh()
            ss.Layout:UpdateScale()

            local profileSplit = { string.split("_", ss.profile.profileName) };
            CustomDropDownMenu_SetSelectedValue(ss.ConfigProfile.ProfileToUseDropDown, profileSplit[2])
            CustomDropDownMenu_SetText(ss.ConfigProfile.ProfileToUseDropDown, profileSplit[2])
          end
        end
      end)
    end,
  }

  -- Add the defaults button
  ss.ConfigProfile.ResetButton = ss.ConfigTools.createButton {
    parent = ss.ConfigProfile,
    name = "ProfileResetButton",
    anch = "BOTTOMRIGHT",
    x = -15,
    y = 15,
    text = ss.T["btn_reset_profile"],
    onClick = function()
      ss.Notification:Confirm(function()
        ss.Utils:ResetSpheres()
      end)
    end,
  }
end
