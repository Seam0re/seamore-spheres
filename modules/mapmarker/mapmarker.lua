--- Map Marker
--
local MapMarker = {}
local AddOnName, nsVars = ...
local ss = nsVars.SS

MapMarker.TEXTURE_HIGHLIGHT = "Interface\\Minimap\\UI-Minimap-ZoomButton-Highlight"
MapMarker.TEXTURE_TRACKING_BORDER = "Interface\\Minimap\\MiniMap-TrackingBorder"
MapMarker.TEXTURE_ICON = ss.FillTextures.get("alternate", 5)

--- Initializes the map marker.
--
function MapMarker:Initialize()
  -- Create map button.
  self.MapButton = ss.MapMarkerButton:New(ss.global.general.MapMarker.locationAngle)

  -- Add the icon.
  self.MapButton.icon = ss.MapMarkerIcon:New(self.MapButton)

  -- Add the trackingBorder.
  self.MapButton.trackingBorder = ss.MapMarkerTrackingBorder:New(self.MapButton)

  -- Set the map marker position on login.
  ss.PendingTask.add("MapButton Reposition", function()
    self.MapButton:Reposition()
  end)
end

--- Toggles the map marker display.
-- @param show Flag to show or hide the frames.
--
function MapMarker:Show(show)
  if self.MapButton and (show == true) then
    self.MapButton:Show()
  elseif show == true then
    self:Initialize()
  elseif self.MapButton then
    self.MapButton:Hide()
  end
end

ss.Utils.addModule("MapMarker", MapMarker)
