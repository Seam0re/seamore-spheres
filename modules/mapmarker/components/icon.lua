--- Map Marker Icon.
--
local MapIcon = {}
local AddOnName, ns = ...
local ss = ns.SS

--- Creates a new map marker icon.
-- @param parent The parent container.
--
function MapIcon:New(parent)
  -- Add the icon
  local icon = parent:CreateTexture("SeamoreSpheres_MinimapButton_Icon", "BACKGROUND")
  icon:SetTexture(ss.MapMarker.TEXTURE_ICON)

  if ss.retailClient then
    icon:SetPoint("TOPLEFT", ss.private.MapMarker.posX, ss.private.MapMarker.posY)
  else
    icon:SetPoint("CENTER", 0, 1) -- Classic
  end

  icon:SetWidth(ss.private.MapMarker.iconSize)
  icon:SetHeight(ss.private.MapMarker.iconSize)
  icon:SetAlpha(1)

  icon:SetVertexColor(ss.private.MapMarker.color.r, ss.private.MapMarker.color.g, ss.private.MapMarker.color.b, ss.private.MapMarker.color.a)
  return icon
end

ss.Utils.addModule("MapMarkerIcon", MapIcon)
