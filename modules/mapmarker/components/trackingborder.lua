--- Map Marker Tracking Border
--
local TrackingBorder = {}
local AddOnName, ns = ...
local ss = ns.SS

--- Creates a new tracking border.
-- @param parent The parent container.
--
function TrackingBorder:New(parent)
  -- Add the trackingBorder
  local trackingBorder = parent:CreateTexture(nil, "OVERLAY")
  trackingBorder:SetTexture(ss.MapMarker.TEXTURE_TRACKING_BORDER)
  trackingBorder:SetWidth(ss.private.MapMarker.trackingBorder.size)
  trackingBorder:SetHeight(ss.private.MapMarker.trackingBorder.size)
  trackingBorder:SetPoint("TOPLEFT", 0, 0)

  return trackingBorder
end

ss.Utils.addModule("MapMarkerTrackingBorder", TrackingBorder)
