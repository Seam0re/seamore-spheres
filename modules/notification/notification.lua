--- Notification Provider
--
local Notification = {}
local AddOnName, nsVars = ...
local ss = nsVars.SS

local table, pairs = table, pairs
local CreateFrame, UIParent = ss.CreateFrame

--local notificationFrame

local notificationFrame = CreateFrame("Frame", ss._f .. "NotificationFrame", UIParent, ss.retailClient and "CustomNotificationDialogTemplate" or "CustomNotificationDialogClassicTemplate")
notificationFrame:Hide()

--- Posts a notification
-- @param title The title to display.
-- @param content The content to display.
--
local function postNotification(title, content)
  --notificationFrame = notificationFrame or CreateFrame("Frame", ss._f .. "NotificationConfirm", UIParent, "CustomNotificationDialogTemplate")
  notificationFrame:SetLayoutType(notificationFrame.LAYOUT_TYPE_NOTIFICATION)

  notificationFrame.Content:SetText(content or "")
  notificationFrame.Title:SetText(title or "")
  notificationFrame:Show()
end

--- Prompts the user for confirmation.
-- @param content The content to display.
-- @param onYes The "Yes" event handler.
-- @param onNo The "No" event handler.
--
local function postConfirm(content, onYes, onNo)
  --notificationFrame = notificationFrame or CreateFrame("Frame", ss._f .. "NotificationConfirm", UIParent, "CustomNotificationDialogTemplate")
  notificationFrame:SetLayoutType(notificationFrame.LAYOUT_TYPE_CONFIRM)

  notificationFrame.Content:SetText(content or ss.T["lbl_are_you_sure"])
  notificationFrame.Title:SetText(ss.T["lbl_confirmation"])
  notificationFrame.onYes = onYes
  notificationFrame.onNo = onNo

  notificationFrame:Show()
end

--- Closes the confirmation dialog.
--
local function closeConfirm()
  if notificationFrame and notificationFrame:IsVisible() then
    notificationFrame.onYes = nil
    notificationFrame.onNo = nil
    notificationFrame:Hide()
  end
end

--- Creates a notification.
-- @param title The title to display.
-- @param content The content to display.
--
function Notification:Notify(title, content)
  if title and content then
    postNotification(title, content)
  elseif self.queue then
    local completed = {}

    for k, v in pairs(self.queue) do
      completed[k] = v
    end

    for k in pairs(completed) do
      table.remove(self.queue, k)
    end
  end
end

--- Creates a confirmation dialog.
-- @param onYes The "Yes" callback.
-- @param onNo The "No" callback.
-- @param content The content to display
--
function Notification:Confirm(onYes, onNo, content)
  postConfirm(content, onYes, onNo)
end

--- Closes the confirmation dialog.
--
function Notification:CloseConfirm()
  closeConfirm()
end

--- Adds a notification to the queue.
-- @param title The title to display.
-- @param content The content to display.
-- @param buttonText The button text.
--
function Notification:Queue(title, content, buttonText)
  self.queue = self.queue or {}
  table.insert(self.queue, title, content, buttonText)
end

ss.Utils.addModule("Notification", Notification)