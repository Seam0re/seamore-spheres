local MicroSphereResourceAnimation = {}
local AddOnName, nsVars = ...
local ss = nsVars.SS

function MicroSphereResourceAnimation:Init()
  local size = self:GetParent():GetWidth() * 2.172
  self:ClearAllPoints()
  self:SetPoint("CENTER", 0, 0)
  self:SetHeight(size)
  self:SetWidth(size)
end

function MicroSphereResourceAnimation:UpdateAnimation(anim, skipVisibility)
  if anim then
    self:SetPosition(0, anim.posX, anim.posY)
    self:SetDisplayInfo(ss.Animations.get(anim.groupId, anim.id))
    self:SetCamDistanceScale(anim.camDistance)
    self.anim = anim

    if not skipVisibility then
      self:SetAlpha(anim.alpha)
      self:Show()
    end
  elseif self.anim then
    self:UpdateAnimation(self.anim, skipVisibility)
  end
end

rawset(_G, "MicroSphereResourceAnimation_OnLoad", function(self)
  for k, v in pairs(MicroSphereResourceAnimation) do
    self[k] = v
  end
end)
