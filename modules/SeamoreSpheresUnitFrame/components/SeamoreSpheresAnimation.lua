local SeamoreSpheresAnimation = {}
local AddOnName, nsVars = ...
local ss = nsVars.SS

local select = select

--- Sets the sphere animation.
-- @param anim The animation details.
--
function SeamoreSpheresAnimation:SetAnimation(anim)
  local x, y = anim.posX or select(2, self:GetPosition()), anim.posY or select(3, self:GetPosition())
  self:SetAlpha(anim.alpha)
  self:SetPosition(0, x, y)
  self:SetCamDistanceScale(anim.camDistance)
  self:SetDisplayInfo(ss.Animations.get(anim.groupId, anim.id))
  self:SetFrameLevel(anim.frameLevel or 3)
end

function SeamoreSpheresAnimation:Init(animationConfig)
  local size = self:GetParent():GetWidth() * 2.172
  self:ClearAllPoints()
  self:SetPoint("CENTER", animationConfig.posX, animationConfig.posY)
  self:SetHeight(size)
  self:SetWidth(size)
end

rawset(_G, "SeamoreSpheresAnimation_OnLoad", function(self)
  for k, v in pairs(SeamoreSpheresAnimation) do
    self[k] = v
  end
end)
