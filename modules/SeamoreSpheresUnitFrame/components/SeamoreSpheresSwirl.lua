local SeamoreSpheresSwirl = {}
local AddOnName, nsVars = ...
local ss = nsVars.SS

local math, pairs = math, pairs

function SeamoreSpheresSwirl:Init(texture, size, color)
  self:SetHeight(size)
  self:SetWidth(size)

  self.texture:SetTexture(texture)
  self.texture:SetVertColor(color)

  self.animGroup.anim.durationPercent = 1
  self.animGroup.anim.durationRemaining = 1
  self.animGroup:Play()
end

function SeamoreSpheresSwirl:SetSpeed(speed, low, high)
  local val = 11 - (speed or 5)
  local duration = math.random(val * low, val * high)
  self.animGroup.anim:SetDuration(duration * self.animGroup.anim.durationPercent)
  self.animGroup.anim.maxDuration = duration
  self.speed = speed
end

function SeamoreSpheresSwirl:SetDurationPercent(percent)
  percent = (percent or 1) + 0.01

  if percent ~= self.animGroup.anim.durationPercent then
    self.animGroup.anim:SetDuration(self.animGroup.anim.maxDuration * percent)
    self.animGroup.anim.durationPercent = percent
  end
end

function SeamoreSpheresSwirl:GetSpeed()
  return self.speed
end

function SeamoreSpheresSwirl:UpdateSwirl(texture, size, color)
  self.texture:SetTexture(texture)
  self:SetHeight(size)
  self:SetWidth(size)
  self.texture:SetVertColor(color)
end

rawset(_G, "SeamoreSpheresSwirl_OnLoad", function(self)
  ss.Extensions:DelegateVertexColor(self.texture)

  for k, v in pairs(SeamoreSpheresSwirl) do
    self[k] = v
  end
end)
