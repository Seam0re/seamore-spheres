--- Global Settings
--
local AddOnName, nsVars = ...
local ss = nsVars.SS

local ssGlobal = ss.dVars.global

local cos, sin = cos, sin

ssGlobal["characters"] = {};

ssGlobal["customResources"] = {
  BuffData = {
    demonhunter = {},
    druid = {},
    mage = {},
    monk = {},
    shaman = {},
    warrior = {},
    deathknight = {},
    hunter = {},
    paladin = {},
    priest = {},
    rogue = {},
    warlock = {},
  },
  DebuffData = {
    demonhunter = {},
    druid = {},
    mage = {},
    monk = {},
    shaman = {},
    warrior = {},
    deathknight = {},
    hunter = {},
    paladin = {},
    priest = {},
    rogue = {},
    warlock = {},
  },
  SpellChargeData = {
    demonhunter = {},
    druid = {},
    mage = {},
    monk = {},
    shaman = {},
    warrior = {},
    deathknight = {},
    hunter = {},
    paladin = {},
    priest = {},
    rogue = {},
    warlock = {},
  },
}

-- global customizable defaults
ssGlobal["general"] = {
  SphereSize = 150,
  MapMarker = {
    locationAngle = 45,
    x = 52 - (80 * cos(45)),
    y = ((80 * sin(45)) - 52),
    show = true,
  },
  scale = 100, -- Deprecated 25-09-2017
  smoothing = nil, -- Deprecated 20-08-2017
  smoothAnimations = "med",
  hideOnCombat = true,
  fadeMicroSpheres = true,
};

ssGlobal["show"] = {
  playerFrame = false,
  artwork = true,
  gradient = true,
};

ssGlobal["track"] = {
  combat = true,
  additionalResource = true,
  pet = true,
  petBattles = true,
};
