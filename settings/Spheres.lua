--- Character Settings
--
local AddOnName, nsVars = ...
local ss = nsVars.SS

local ssProfile = ss.dVars.spheres

ssProfile["microResourceFillTextures"] = {
  [1] = "diablo3",
  [2] = "diablo3",
};

ssProfile["microResourceFillTextureGroups"] = {
  [1] = "primary",
  [2] = "primary",
};

ssProfile["modeInfo"] = {
  health = {
    mode = "default",
    modeSpec = "defaultSpec",
  },
  mainResource = {
    mode = "default",
    modeSpec = "defaultSpec",
  },
  additionalResource = {
    mode = "default",
    modeSpec = "defaultSpec",
  },
  pet = {
    mode = "default",
    modeSpec = "defaultSpec",
  },
  vehicle = {
    mode = "default",
    modeSpec = "defaultSpec",
  },
  vehicleResource = {
    mode = "default",
    modeSpec = "defaultSpec",
  },
};
