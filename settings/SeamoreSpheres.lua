--- Private Settings
--
local AddOnName, nsVars = ...
local ss = nsVars.SS

local ssPrivate = ss.pVars.spheres

ssPrivate["font"] = {
  general = ss.SharedMedia.fontChoices[1],
  sphere = ss.SharedMedia.fontChoices[2],
};

ssPrivate["fontFlag"] = {
  sphere = "THINOUTLINE",
};

ssPrivate["fontSize"] = {
  sphereSmall = 14,
  sphereMedium = 16,
  sphereLarge = 28,
};

ssPrivate["general"] = {
  SphereSize = 150,
  resources = {
    Health = { 0, { r = 0.00, g = 1.00, b = 0.00, a = 1 }, },
    Mana = { 0, { r = 0.00, g = 0.00, b = 1.00, a = 1 }, },
    Absorb = { r = 0.00, g = 1.00, b = 0.55, a = 0.5 },
  },
};

ssPrivate["layout"] = {
  config = {
    frameLevelAdjust = 6,
  },
  frameList = {
    [1] = "health",
    [2] = "mainResource",
    [3] = "additionalResource",
    [4] = "additionalResource",
  },
};

ssPrivate["MapMarker"] = {
  size = 33,
  iconSize = 21,
  posX = 7,
  posY = -6,
  texture = ss.FillTextures.get("alternate", 5),
  color = { r = 1, g = 0, b = 0, a = 1 },
  trackingBorder = {
    size = 56,
    texture = "Interface\\Minimap\\MiniMap-TrackingBorder",
  },
};

ssPrivate["SSProfile_Default"] = {
  health = {
    general = {
      size = 150,
      scale = 1,
      posX = 175,
      posY = 0,
      anchor = "BOTTOMLEFT",
      unit = "player",
      configName = "health",
      frameStrata = "BACKGROUND",
      name = "SeamoreSpheres_Health",
      globalScale = 100,
    },
    animation = {
      posX = 0,
      posY = 0.07,
      alpha = 0.3,
      camDistance = 1.85,
      scale = 1,
      id = "pearl",
      groupId = "orbtacular",
      frameLevel = 3,
    },
    font = {
      [1] = {
        show = true,
      },
      [2] = {
        show = true,
      },
    },
    textFormat = "truncated",
    modeSpec = "defaultSpec",
    mode = "default",
    modes = {
      defaultSpec = {
        default = {
          fillColor = {
            [1] = { 0, ss.ClassResourceData.classColor(ss.curClass), },
          },
          swirlColor = {
            [1] = ss.ClassResourceData.classColor(ss.curClass),
          },
          fontColor = {
            [1] = { r = 1, g = 1, b = 1, a = 1 },
            [2] = { r = 1, g = 1, b = 1, a = 1 },
          },
          fillInfo = {
            fillGroup = ss.FillTextures.defaultGroup,
            fillId = ss.FillTextures.defaultId,
          },
          fillTexture = {
            [1] = ss.FillTextures.defaultTexture,
          },
          monitorValue = 0,
          swirl = {
            id = "galaxy",
            groupId = "swirl",
            speed = 5,
          },
        },
        combat = {
          fillColor = {
            [1] = { 0, ss.ClassResourceData.classColor(ss.curClass), },
          },
          swirlColor = {
            [1] = { r = 1.000, g = 0.000, b = 0.000, a = 0.35 },
          },
          fontColor = {
            [1] = { r = 1, g = 1, b = 1, a = 1 },
            [2] = { r = 1, g = 1, b = 1, a = 1 },
          },
          fillInfo = {
            fillGroup = ss.FillTextures.defaultGroup,
            fillId = ss.FillTextures.defaultId,
          },
          fillTexture = {
            [1] = ss.FillTextures.defaultTexture,
          },
          monitorValue = 0,
          swirl = {
            id = "galaxy",
            groupId = "swirl",
            speed = 5,
          },
        },
      },
    },
  },
  mainResource = {
    general = {
      size = 150,
      scale = 1,
      posX = -175,
      posY = 0,
      anchor = "BOTTOMRIGHT",
      unit = "player",
      configName = "mainResource",
      frameStrata = "BACKGROUND",
      name = "SeamoreSpheres_MainResource",
      globalScale = 100,
    },
    animation = {
      posX = 0,
      posY = 0.07,
      alpha = 0.3,
      camDistance = 1.85,
      scale = 1,
      id = "pearl",
      groupId = "orbtacular",
      frameLevel = 3,
    },
    font = {
      [1] = {
        show = true,
      },
      [2] = {
        show = true,
      },
    },
    textFormat = "truncated",
    modeSpec = "defaultSpec",
    mode = "default",
    modes = {
      defaultSpec = {
        default = {
          fillColor = {
            [1] = { 0, ss.ClassResourceData.classColor(ss.curClass), },
          },
          swirlColor = {
            [1] = ss.ClassResourceData.classColor(ss.curClass),
          },
          fontColor = {
            [1] = { r = 1, g = 1, b = 1, a = 1 },
            [2] = { r = 1, g = 1, b = 1, a = 1 },
          },
          fillInfo = {
            fillGroup = ss.FillTextures.defaultGroup,
            fillId = ss.FillTextures.defaultId,
          },
          fillTexture = {
            [1] = ss.FillTextures.defaultTexture,
          },
          monitorValue = 0,
          swirl = {
            id = "galaxy",
            groupId = "swirl",
            speed = 5,
          },
        },
      },
    },
  },
  additionalResource = {
    general = {
      size = 87,
      scale = 1.724,
      posX = -50,
      posY = 125,
      anchor = "BOTTOMRIGHT",
      show = true,
      showValue = true,
      unit = "player",
      configName = "additionalResource",
      frameStrata = "BACKGROUND",
      name = "SeamoreSpheres_AdditionalResource",
      globalScale = 100,
    },
    animation = {
      posX = 0,
      posY = 0.07,
      alpha = 0.3,
      camDistance = 1.85,
      scale = 1,
      id = "pearl",
      groupId = "orbtacular",
      frameLevel = 3,
    },
    font = {
      [1] = {
        show = true,
      },
      [2] = {
        show = true,
      },
    },
    textFormat = "truncated",
    modeSpec = "defaultSpec",
    mode = "default",
    modes = {
      defaultSpec = {
        default = {
          fillColor = {
            [1] = { 0, { r = 1.000, g = 0.000, b = 0.000, a = 1.00 }, },
          },
          swirlColor = {
            [1] = { r = 1.000, g = 0.000, b = 0.000, a = 0.35 },
          },
          fontColor = {
            [1] = { r = 1, g = 1, b = 1, a = 1 },
            [2] = { r = 1, g = 1, b = 1, a = 1 },
          },
          fillInfo = {
            fillGroup = ss.FillTextures.defaultGroup,
            fillId = ss.FillTextures.defaultId,
          },
          fillTexture = {
            [1] = ss.FillTextures.defaultTexture,
          },
          monitorValue = 0,
          swirl = {
            id = "galaxy",
            groupId = "swirl",
            speed = 5,
          },
        },
      },
    },
  },
  pet = {
    general = {
      size = 87,
      scale = 1.724,
      posX = 50,
      posY = 125,
      anchor = "BOTTOMLEFT",
      show = true,
      showValue = true,
      unit = "pet",
      configName = "pet",
      frameStrata = "BACKGROUND",
      name = "SeamoreSpheres_PetResource",
      globalScale = 100,
    },
    animation = {
      posX = 0,
      posY = 0.07,
      alpha = 0.3,
      camDistance = 1.85,
      scale = 1,
      id = "pearl",
      groupId = "orbtacular",
      frameLevel = 3,
    },
    font = {
      [1] = {
        show = true,
      },
      [2] = {
        show = true,
      },
    },
    textFormat = "truncated",
    modeSpec = "defaultSpec",
    mode = "default",
    modes = {
      defaultSpec = {
        default = {
          fillColor = {
            [1] = { 0, { r = 0.67, g = 0.83, b = 0.45, a = 1, }, },
            [2] = { 0, { r = 1.000, g = 0.500, b = 0.250, a = 1 }, },
          },
          swirlColor = {
            [1] = { r = 0.67, g = 0.83, b = 0.45, a = 1, },
          },
          fontColor = {
            [1] = { r = 1, g = 1, b = 1, a = 1 },
            [2] = { r = 1, g = 1, b = 1, a = 1 },
          },
          fillInfo = {
            fillGroup = "split",
            fillId = 1,
          },
          fillTexture = ss.FillTextures.get("split", 1),
          monitorValue = 0,
          swirl = {
            id = "galaxy",
            groupId = "swirl",
            speed = 5,
          },
        },
      },
    },
  },
  vehicle = {
    general = {
      size = 150,
      scale = 1,
      posX = 175,
      posY = 0,
      anchor = "BOTTOMLEFT",
      unit = "vehicle",
      configName = "vehicle",
      frameStrata = "BACKGROUND",
      name = "SeamoreSpheres_Vehicle",
      globalScale = 100,
    },
    animation = {
      posX = 0,
      posY = 0.07,
      alpha = 0.3,
      camDistance = 1.85,
      scale = 1,
      id = "pearl",
      groupId = "orbtacular",
      frameLevel = 3,
    },
    font = {
      [1] = {
        show = true,
      },
      [2] = {
        show = true,
      },
    },
    textFormat = "truncated",
    modeSpec = "defaultSpec",
    mode = "default",
    modes = {
      defaultSpec = {
        default = {
          fillColor = {
            [1] = { 0, { r = 0.67, g = 0.83, b = 0.45, a = 1, }, },
          },
          swirlColor = {
            [1] = { r = 0.67, g = 0.83, b = 0.45, a = 1, },
          },
          fontColor = {
            [1] = { r = 1, g = 1, b = 1, a = 1 },
            [2] = { r = 1, g = 1, b = 1, a = 1 },
          },
          fillInfo = {
            fillGroup = ss.FillTextures.defaultGroup,
            fillId = ss.FillTextures.defaultId,
          },
          fillTexture = {
            [1] = ss.FillTextures.defaultTexture,
          },
          monitorValue = 0,
          swirl = {
            id = "galaxy",
            groupId = "swirl",
            speed = 5,
          },
        },
        combat = {
          fillColor = {
            [1] = { 0, { r = 0.67, g = 0.83, b = 0.45, a = 1, }, },
          },
          swirlColor = {
            [1] = { r = 1.000, g = 0.000, b = 0.000, a = 0.35 },
          },
          fontColor = {
            [1] = { r = 1, g = 1, b = 1, a = 1 },
            [2] = { r = 1, g = 1, b = 1, a = 1 },
          },
          fillInfo = {
            fillGroup = ss.FillTextures.defaultGroup,
            fillId = ss.FillTextures.defaultId,
          },
          fillTexture = {
            [1] = ss.FillTextures.defaultTexture,
          },
          monitorValue = 0,
          swirl = {
            id = "galaxy",
            groupId = "swirl",
            speed = 5,
          },
        },
      },
    },
  },
  vehicleResource = {
    general = {
      size = 150,
      scale = 1,
      posX = -175,
      posY = 0,
      anchor = "BOTTOMRIGHT",
      unit = "vehicle",
      configName = "vehicleResource",
      frameStrata = "BACKGROUND",
      name = "SeamoreSpheres_VehicleResource",
      globalScale = 100,
    },
    animation = {
      posX = 0,
      posY = 0.07,
      alpha = 0.3,
      camDistance = 1.85,
      scale = 1,
      id = "pearl",
      groupId = "orbtacular",
      frameLevel = 3,
    },
    font = {
      [1] = {
        show = true,
      },
      [2] = {
        show = true,
      },
    },
    textFormat = "truncated",
    modeSpec = "defaultSpec",
    mode = "default",
    modes = {
      defaultSpec = {
        default = {
          fillColor = {
            [1] = { 0, { r = 1.000, g = 0.500, b = 0.250, a = 1 }, },
          },
          swirlColor = {
            [1] = { r = 1.000, g = 0.500, b = 0.250, a = 1 },
          },
          fontColor = {
            [1] = { r = 1, g = 1, b = 1, a = 1 },
            [2] = { r = 1, g = 1, b = 1, a = 1 },
          },
          fillInfo = {
            fillGroup = ss.FillTextures.defaultGroup,
            fillId = ss.FillTextures.defaultId,
          },
          fillTexture = {
            [1] = ss.FillTextures.defaultTexture,
          },
          monitorValue = 0,
          swirl = {
            id = "galaxy",
            groupId = "swirl",
            speed = 5,
          },
        },
        combat = {
          fillColor = {
            [1] = { 0, { r = 1.000, g = 0.500, b = 0.250, a = 1 }, },
          },
          swirlColor = {
            [1] = { r = 1.000, g = 0.500, b = 0.250, a = 1 },
          },
          fontColor = {
            [1] = { r = 1, g = 1, b = 1, a = 1 },
            [2] = { r = 1, g = 1, b = 1, a = 1 },
          },
          fillInfo = {
            fillGroup = ss.FillTextures.defaultGroup,
            fillId = ss.FillTextures.defaultId,
          },
          fillTexture = {
            [1] = ss.FillTextures.defaultTexture,
          },
          monitorValue = 0,
          swirl = {
            id = "galaxy",
            groupId = "swirl",
            speed = 5,
          },
        },
      },
    },
  },
};

ssPrivate["textures"] = {
  gloss = ss.SphereTextures.get("gloss", 3),
  mainResourceArtwork = ss.SharedMedia   .getArtwork(1, "angel"),
  healthArtwork = ss.SharedMedia   .getArtwork(1, "demon"),
  divider = ss.SphereTextures.get("divider", 1),
};

-- camD 0.589, x 0, y 0

--[[
--
 -
 obrtacular - dwarf artifact
  cam distance = 0.589
  x = 0
  y = 0
 orbtacular - purple circus
  cam distance = 0.708
  x = 0
  y = -0.06





 -
-- ]]



