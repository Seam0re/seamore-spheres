--- Media listing for all fill textures
--
local FillTextures = {}
local AddOnName, nsVars = ...
local ss = nsVars.SS

--[[
  Instructions for Creating New Textures.
    Texture Size: 256 x 256
    Image   Size: 255 x 255
    Circle  Size: 228 x 228

    - Create a new texture, 256 x 256, with an alpha channel and clear background.
    - Select a texture of any size greater than 228 x 228.
    - Scale the texture to 255 x 255.
    - Crop a cicular area with a diameter of 228.
    - Copy selected area into the new texture file.
    - Center align selected area.
    - Desaturate the texture to remove colors.
    - Adjust brightness and contrast as desired.
    - Export texture to .tga format.

    Swirl Textures:
      - Once desaturated, change the color #000000 (black) to clear.
      - This provides 1 of 3 required swirl textures.
      - Resize, reuse or replace this texture for the remaining textures.
      - Declare as table with indexes 1, 2 and 3.
      - Rearrange values as desired.

    Swirl Textures (alternate):
      - Before desaturation, select by color to determine swirl.
      - Create a new texture as before using selected area.
      - Desaturate the texture to remove colors.
      - Create and declare new texture as above.
 ]]

local type, pairs, next, string = type, pairs, next, string

FillTextures.defaultGroup = "primary"
FillTextures.defaultId = "fire ring"
FillTextures.defaultTexture = ss.SharedMedia.textureLocation .. "fire.tga"

FillTextures.textures = {
  primary = {
    ["bubbles"] = "roth_orb_filling11.tga",
    ["darkstar"] = "roth_orb_filling14.tga",
    ["diablo3"] = "roth_orb_filling15.tga",
    ["dominion"] = "roth_orb_filling19.tga",
    ["earth"] = "roth_orb_filling2.tga",
    ["exile"] = "roth_orb_filling18.tga",
    ["fraktal circle"] = "roth_orb_filling6.tga",
    ["fubble"] = "roth_orb_filling16.tga",
    ["galaxy"] = "roth_orb_filling4.tga",
    ["golf"] = "roth_orb_filling13.tga",
    ["gradient"] = "roth_orb_filling10.tga",
    ["icecream"] = "roth_orb_filling8.tga",
    ["jupiter"] = "roth_orb_filling5.tga",
    ["marble"] = "roth_orb_filling9.tga",
    ["mars"] = "roth_orb_filling3.tga",
    ["moon"] = "roth_orb_filling1.tga",
    ["runes"] = "roth_orb_filling20.tga",
    ["sigil orb"] = "roth_orb_filling21.tga",
    ["silver"] = "roth_orb_filling17.tga",
    ["sun"] = "roth_orb_filling7.tga",
    ["swirl"] = "roth_orb_filling22.tga",
    ["woodpepples"] = "roth_orb_filling12.tga",
    ["ghostly"] = "ghostly.tga",
    ["fire ring"] = "fire.tga",
  },
  alternate = {
    [1] = "orb_filling1.tga",
    [2] = "orb_filling2.tga",
    [3] = "orb_filling3.tga",
    [4] = "orb_filling4.tga",
    [5] = "orb_filling5.tga",
    [6] = "orb_filling6.tga",
    [7] = "orb_filling7.tga",
  },
  split = {
    [1] = {
      [1] = "petFilling_health.tga",
      [2] = "petFilling_power.tga",
    },
  },
  swirl = {
    ["galaxy"] = {
      [1] = "orb_rotation_galaxy1.tga",
      [2] = "orb_rotation_galaxy2.tga",
      [3] = "orb_rotation_galaxy3.tga",
    },
    ["galaxy2"] = {
      [1] = "galaxy.tga",
      [2] = "galaxy2.tga",
      [3] = "galaxy4.tga",
    },
    ["bubbles"] = {
      [1] = "orb_rotation_bubbles1.tga",
      [2] = "orb_rotation_bubbles2.tga",
      [3] = "orb_rotation_bubbles3.tga",
    },
    ["iris"] = {
      [1] = "orb_rotation_iris1.tga",
      [2] = "orb_rotation_iris2.tga",
      [3] = "orb_rotation_iris3.tga",
    },
    ["tron tunnel"] = {
      [1] = "swirl\\tron_tunnel_crop_1.tga",
      [2] = "swirl\\tron_tunnel_crop_2.tga",
      [3] = "swirl\\tron_tunnel_crop_3.tga",
    },
    ["neuron chaos"] = {
      [1] = "swirl\\neuron_chaos_crop_1.tga",
      [2] = "swirl\\neuron_chaos_crop_2.tga",
      [3] = "swirl\\neuron_chaos_crop_3.tga",
    },
    ["clouds"] = {
      [1] = "swirl\\clouds_crop_1.tga",
      [2] = "swirl\\clouds_crop_2.tga",
      [3] = "swirl\\clouds_crop_3.tga",
    },
    ["wavy spiral"] = {
      [1] = "swirl\\wavy_spiral_crop_1.tga",
      [2] = "swirl\\wavy_spiral_crop_2.tga",
      [3] = "swirl\\wavy_spiral_crop_3.tga",
    },
    ["design metal"] = {
      [1] = "swirl\\design_metal_crop_1.tga",
      [2] = "swirl\\design_metal_crop_2.tga",
      [3] = "swirl\\design_metal_crop_3.tga",
    },
    ["water droplets"] = {
      [1] = "swirl\\water_droplets_crop_1.tga",
      [2] = "swirl\\water_droplets_crop_2.tga",
      [3] = "swirl\\water_droplets_crop_3.tga",
    },
    ["fire"] = {
      [1] = "swirl\\fire_crop_1.tga",
      [2] = "swirl\\fire_crop_2.tga",
      [3] = "swirl\\fire_crop_3.tga",
    },
    ["fractal droplet"] = {
      [1] = "swirl\\fractal_droplet_crop_1.tga",
      [2] = "swirl\\fractal_droplet_crop_2.tga",
      [3] = "swirl\\fractal_droplet_crop_3.tga",
    },
    ["ghostly"] = {
      [1] = "swirl\\ghostly_crop_1.tga",
      [2] = "swirl\\ghostly_crop_2.tga",
      [3] = "swirl\\ghostly_crop_3.tga",
    },
    ["flare"] = {
      [1] = "swirl\\flare_swirl_1.tga",
      [2] = "swirl\\flare_swirl_2.tga",
      [3] = "swirl\\flare_swirl_3.tga",
    },
    ["star"] = {
      [1] = "swirl\\wheel_star_2.tga",
      [2] = "swirl\\wheel_star_2.tga",
      [3] = "swirl\\star_2.tga",
    },
    ["wheel"] = {
      [1] = "swirl\\wheel_star_3.tga",
      [2] = "swirl\\cross_star.tga",
      [3] = "swirl\\cross_star.tga",
    },
  },
  metal = {
    ["aluminum wall plating"] = "metal\\aluminum_wall_plating.tga",
    ["brushed metal"] = "metal\\brushed_metal.tga",
    ["brushed silver"] = "metal\\brushed_silver.tga",
    ["brushed steel"] = "metal\\brushed_steel.tga",
    ["chains"] = "metal\\chains.tga",
    ["copper"] = "metal\\copper.tga",
    ["diamond plate metal"] = "metal\\diamond_plate_metal.tga",
    ["galvanized dark steel"] = "metal\\galvanized_dark_steel.tga",
    ["galvanized steel"] = "metal\\galvanized_steel.tga",
    ["galvanized steel 2"] = "metal\\galvanized_steel_2.tga",
    ["grafiti"] = "metal\\grafiti.tga",
    ["iron door"] = "metal\\iron_door.tga",
    ["iron plate"] = "metal\\iron_plate.tga",
    ["metal shavings"] = "metal\\metal_shavings.tga",
    ["metal tube"] = "metal\\metal_tube.tga",
    ["metallic"] = "metal\\metallic.tga",
    ["oxidized steel iron"] = "metal\\oxidized_steel_iron.tga",
    ["pitted copper"] = "metal\\pitted_copper.tga",
    ["polished iron slab"] = "metal\\polished_iron_slab.tga",
    ["polished iron slab 2"] = "metal\\polished_iron_slab_2.tga",
    ["rust"] = "metal\\rust.tga",
    ["rust metal stock"] = "metal\\rust_metal_stock.tga",
    ["rusted heavy"] = "metal\\rusted_heavy.tga",
    ["rusted metal pipe"] = "metal\\rusted_metal_pipe.tga",
    ["rusted paint"] = "metal\\rusted_paint.tga",
    ["rusted wet metal"] = "metal\\rusted_wet_metal.tga",
    ["rusty rippled"] = "metal\\rusty_rippled.tga",
    ["sanded swirl steel"] = "metal\\sanded_swirl_steel.tga",
    ["scratched dark"] = "metal\\scratched_dark.tga",
    ["spin aluminum"] = "metal\\spin_aluminum.tga",
    ["wave brush steel"] = "metal\\wave_brush_steel.tga",
  },
}

FillTextures.remoteAssets = { -- Placeholder for remote assets.
}


--- Retrieves a texture location.
-- @param group The texture group.
-- @param index The texture index.
-- @return The texture.
--
function FillTextures.get(group, index)
  if FillTextures.textures[group] then
    if FillTextures.textures[group][index] and (type(FillTextures.textures[group][index]) == "table") then
      local textures = {}

      for i, v in pairs(FillTextures.textures[group][index]) do
        textures[i] = string.match(v, "Interface\\AddOns") and v or ss.SharedMedia.textureLocation .. v
      end

      return textures
    elseif FillTextures.textures[group] then
      return ss.SharedMedia.textureLocation .. FillTextures.textures[group][index]
    end
  elseif ss.FillTextures.remoteAssets[group] then
    return ss.FillTextures.remoteAssets[group][index]
  else
    --ss.Utils:printf(ss.T["error_load_texture"], index or "", group or "")
    return FillTextures.textures[FillTextures.defaultGroup][FillTextures.defaultId]
  end
end

--- Returns the proper size for a swirl texture.
-- @param index The swirl group identifier.
-- @param targetSize The current size.
-- @return The adjusted size.
--
function FillTextures.getSwirlSize(index, targetSize)
  if (index == "galaxy") or (index == "galaxy2") then
    return targetSize
  elseif (index == "bubbles") or (index == "iris") or (index == "flare") then
    return (targetSize * 1.1)
  else
    return (targetSize * 1.2)
  end
end

--- Provides a list of texture identifiers within a group.
-- @param group The group's identifier.
-- @return The texture list.
--
function FillTextures.list(group)
  if FillTextures.textures[group] then
    return FillTextures.textures[group]
  elseif ss.FillTextures.remoteAssets[group] then
    return ss.FillTextures.remoteAssets[group]
  end
end

--- Lists all texture groups and their texture idnentifiers.
-- @return The complete list of textures.
--
function FillTextures.listAll()
  local list = {}
  list.primary = FillTextures.textures["primary"]
  list.metal = FillTextures.textures["metal"]

  for k, v in pairs(ss.FillTextures.remoteAssets) do
    if next(v) ~= nil then
      list[k] = v
    end
  end

  return list
end

--- Adds remote textures to a table.
-- @param targetList The target table.
--
function FillTextures.addRemoteFillTextures(targetList)
  for k, v in pairs(ss.FillTextures.remoteAssets) do
    if next(v) ~= nil then
      targetList[k] = v
    end
  end
end

ss.Utils.addModule("FillTextures", FillTextures)
