--- Required properties and miscellaneous media
--
local SharedMedia = {}
local AddOnName, nsVars = ...
local ss = nsVars.SS

local fontLocation = "Interface\\Addons\\SeamoreSpheres\\media\\fonts\\"

SharedMedia.textureLocation = "Interface\\AddOns\\SeamoreSpheres\\media\\textures\\"

SharedMedia.fontChoices = {
  [1] = fontLocation .. "Journal\\journal.ttf",
  [2] = "Fonts\\ARIALN.TTF",
  [3] = "Fonts\\FRIZQT__.TTF",
  [4] = "Fonts\\skurri.TTF",
  [5] = "Fonts\\MORPHEUS.TTF",
  exo2 = {
    ["Black"] = fontLocation .. "exo-2\\Exo2-Black.otf",
    ["BlackItalic"] = fontLocation .. "exo-2\\Exo2-BlackItalic.otf",
    ["Bold"] = fontLocation .. "exo-2\\Exo2-Bold.otf",
    ["BoldItalic"] = fontLocation .. "exo-2\\Exo2-BoldItalic.otf",
    ["ExtraBold"] = fontLocation .. "exo-2\\Exo2-ExtraBold.otf",
    ["ExtraBoldItalic"] = fontLocation .. "exo-2\\Exo2-ExtraBoldItalic.otf",
    ["ExtraLight"] = fontLocation .. "exo-2\\Exo2-ExtraLight-.otf",
    ["ExtraLightItalic"] = fontLocation .. "exo-2\\Exo2-ExtraLightItalic.otf",
    ["Italic"] = fontLocation .. "exo-2\\Exo2-Italic.otf",
    ["Light"] = fontLocation .. "exo-2\\Exo2-Light.otf",
    ["LightItalic"] = fontLocation .. "exo-2\\Exo2-LightItalic.otf",
    ["Medium"] = fontLocation .. "exo-2\\Exo2-Medium.otf",
    ["MediumItalic"] = fontLocation .. "exo-2\\Exo2-MediumItalic.otf",
    ["Regular"] = fontLocation .. "exo-2\\Exo2-Regular.otf",
    ["SemiBold"] = fontLocation .. "exo-2\\Exo2-SemiBold.otf",
    ["SemiBoldItalic"] = fontLocation .. "exo-2\\Exo2-SemiBoldItalic.otf",
    ["Thin"] = fontLocation .. "exo-2\\Exo2-Thin.otf",
    ["ThinItalic"] = fontLocation .. "exo-2\\Exo2-ThinItalic.otf",
  },
  daniel = {
    ["black"] = fontLocation .. "daniel\\Daniel-Black.otf",
    ["bold"] = fontLocation .. "daniel\\Daniel-Bold.otf",
    ["regular"] = fontLocation .. "daniel\\Daniel-Regular.otf",
  },
  journal = {
    ["regular"] = fontLocation .. "Journal\\journal.ttf",
  },
  jrhand = {
    ["regular"] = fontLocation .. "Jr-Hand\\jrha___.ttf",
  },
  burlingtonScript = {
    ["regular"] = fontLocation .. "SF-Burlington-Script\\SF_Burlington_Script.ttf",
    ["bold"] = fontLocation .. "SF-Burlington-Script\\SF_Burlington_Script_Bold.ttf",
    ["italic"] = fontLocation .. "SF-Burlington-Script\\SF_Burlington_Script_Italic.ttf",
    ["boldItalic"] = fontLocation .. "SF-Burlington-Script\\SF_Burlington_Script_Bold_Italic.ttf",
  },
}

SharedMedia.artworkTextures = {
  [1] = {
    angel = "d3_angel2test.tga",
    demon = "d3_demon2test.tga",
  },
  [2] = {
    angel = "d3_angel2.tga",
    demon = "d3_demon2.tga",
  },
}

SharedMedia.miscTextures = {
  gradientCircle = "Interface\\AddOns\\SeamoreSpheres\\media\\textures\\test\\gradient_circle.tga",
  gradientSquare = "Interface\\AddOns\\SeamoreSpheres\\media\\textures\\test\\gradient_square.tga",
  gradientCircleLg = "Interface\\AddOns\\SeamoreSpheres\\media\\textures\\test\\gradient_circle_50.tga",
  gradientSquareLg = "Interface\\AddOns\\SeamoreSpheres\\media\\textures\\test\\gradient_square_50.tga",
}

--- Return an artwork texture.
-- @param group The texture group.
-- @param index The texture index
-- @return the texture.
--
function SharedMedia.getArtwork(group, index)
  return SharedMedia.textureLocation .. SharedMedia.artworkTextures[group][index]
end

ss.Utils.addModule("SharedMedia", SharedMedia)
