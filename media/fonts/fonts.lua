
local AddOnName, nsVars = ...
local ss = nsVars.SS

local FontObjects = {
  Exo2Medium12 = "Exo2Medium12",
  Exo2Medium14 = "Exo2Medium14",
  Exo2Medium16 = "Exo2Medium16",
  Exo2Medium18 = "Exo2Medium18",
  Exo2Medium28 = "Exo2Medium28",
  Exo2Medium14Outline = "Exo2Medium14Outline",
  Exo2Medium16Outline = "Exo2Medium16Outline",
  Exo2Medium28Outline = "Exo2Medium28Outline",
  Exo2SemiBold14 = "Exo2SemiBold14",
  Exo2SemiBold30 = "Exo2SemiBold30",
}

ss.Utils.addModule("FontObjects", FontObjects)
