--- Media listing for base sphere components
--
local SphereTextures = {}
local AddOnName, nsVars = ...
local ss = nsVars.SS

SphereTextures.textures = {
  gloss = {
    [1] = "orb_gloss.tga",
    [2] = "orb_gloss2.tga",
    [3] = "orb_gloss3.tga",
  },
  divider = {
    [1] = "middleBar.tga",
  },
}

--- Retrieves a texture location.
-- @param group The texture group.
-- @param index The texture index.
-- @return The texture.
--
function SphereTextures.get(group, index)
  return ss.SharedMedia.textureLocation .. SphereTextures.textures[group][index]
end

ss.Utils.addModule("SphereTextures", SphereTextures)
