--- Media listing for all DisplayID animations
--
local Animations = {}
local AddOnName, nsVars = ...
local ss = nsVars.SS

Animations.animations = {
    orbtacular = {
      ["purble flash orb"] = 48109,
      ["blue flash orb"]   = 48106,
      ["pearl"]            = 32368,
      ["white flower"]     = 53769,
      ["the planet"]       = 44652,
      ["red chocolate"]    = 47882,
      ["purple chocolate"] = 48254,
      ["red magnet"]       = 33853,
      ["white magnet"]     = 34404,
      ["dwarf artifact"]   = 38699,
      ["water planet"]     = 20782,
      ["sahara"]           = 25392,
      ["cthun"]            = 37867,
      ["purple circus"]    = 39108,
      ["spore"]            = 16111,
      ["snow ball"]        = 22707,
      ["death ball"]       = 29308,
      ["sphere"]           = 25889,
      ["titan sphere"]     = 28741,
      ["force sphere"]     = 42486,
      ["soulshard"]        = 45414,
      ["arcane orb"]       = 55036,
      ["purple orb"]       = 55948,
      ["brown chocolate"]  = 56632,
      ["rage orb"]         = 60225,
    },
    fog = {
      ["red fog"]       = 17010,
      ["purple fog"]    = 17054,
      ["green fog"]     = 17055,
      ["yellow fog"]    = 17286,
      ["turquoise fog"] = 18075,
      ["white fog"]     = 23343,
      ["orb fog"]       = 58827,
    },
    portal = {
      ["red portal"]     = 23422,
      ["blue portal"]    = 34319,
      ["purple portal"]  = 34645,
      ["warlock portal"] = 29074,
      ["soul harvest"]   = 40645,
      ["yellow portal"]  = 52396,
      ["blue portal 2"]  = 58948,
    },
    swirly = {
      ["blue rune swirly"] = 27393,
      ["purple swirly"]    = 28460,
      ["blue swirly"]      = 29561,
      ["white swirly"]     = 29286,
      ["magic swirly"]     = 39581,
      ["swirly cloud"]     = 23310,
      ["white cloud"]      = 55752,
    },
    fire = {
      ["green fire"]  = 27625,
      ["fire"]        = 38327,
      ["green vapor"] = 28639,
      ["skull"]       = 53026,
    },
    elemental = {
      ["purple naaru"]          = 17065,
      ["white naaru"]           = 17871,
      ["fire elemental"]        = 1070,
      ["purple elemental"]      = 14252,
      ["white elemental"]       = 19003,
      ["light-green elemental"] = 15406,
      ["red elemental"]         = 17045,
      ["green elemental"]       = 9449,
      ["shadow elemental"]      = 4629,
      ["glow elemental"]        = 22731,
      ["void walker"]           = 16635,
      ["green ghost"]           = 16170,
      ["fire guard"]            = 24905,
      ["flaming bone guard"]    = 31450,
      ["bound fire elemental"]  = 34942,
      ["bound water elemental"] = 36345,
      ["wod fire elemental"]    = 53456,
      ["magma elemental"]       = 58534,
    },
    crystal = {
      ["blue crystal"]   = 17856,
      ["green crystal"]  = 20900,
      ["purple crystal"] = 22506,
      ["health crystal"] = 26419,
      ["power crystal"]  = 26418,
    },
    aquarium = {
      ["fish"]   = 4878,
      ["deviat"] = 7449,
    },
    unique = {
      ["diablo"]              = 10992,
      ["murcablo"]            = 38803,
      ["blue angel"]          = 5233,
      ["alarm-o-bot"]         = 6888,
      ["molten giant"]        = 11986,
      ["magma giant"]         = 38594,
      ["algalon"]             = 28641,
      ["spectral bear"]       = 31094,
      ["mimiron head"]        = 28890,
      ["therazane"]           = 32913,
      ["fire kitten"]         = 38150,
      ["orange fire helldog"] = 38187,
      ["blue fire helldog"]   = 38189,
      ["deathwing lava claw"] = 39354,
      ["cranegod"]            = 40924,
      ["burning blob"]        = 38548,
      ["blue wisp"]           = 1824,
      ["robot"]               = 49084,
    },
    sparkling = {
      ["spark"]            = 26753,
      ["yellow spark"]     = 27617,
      ["creme spark"]      = 51486,
      ["purple splash"]    = 46920,
      ["yellow plasma"]    = 48210,
      ["strobo"]           = 29612,
      ["strobo2"]          = 41110,
      ["strobo3"]          = 44465,
      ["hammer of wrath"]  = 30792,
      ["aqua spark"]       = 47891,
      ["orb spark"]        = 57891,
      ["yellow orb spark"] = 60361,
    },
    spirit = {
      ["killrock eye"]     = 2421,
      ["darkmoon eye"]     = 46174,
      ["blue soul"]        = 21485,
      ["red soul"]         = 30150,
      ["red spirit"]       = 39740,
      ["blue spirit"]      = 39738,
      ["ghost skull"]      = 28089,
      ["red snake spirit"] = 51370,
    },
    environment = {
      ["fire flower"]       = 35741,
      ["oasis flower"]      = 36405,
      ["icethorn flower"]   = 42785,
      ["purple mushroom"]   = 39014,
      ["purple lantern"]    = 36901,
      ["turquoise lantern"] = 36902,
      ["purple egg"]        = 31905,
      ["orange egg"]        = 34010,
      ["bubble torch"]      = 26506,
      ["onyx statue"]       = 41853,
      ["melting pot"]       = 51406,
      ["white flower"]      = 53769,
    },
  }

--- Retrives an animation location.
-- @param group The animation group.
-- @param index The animation index.
-- @return The animation.
--
function Animations.get(group, index)
  return Animations.animations[group][index]
end

--- Provides a list of animation identifiers within a group.
-- @param group The group's identifier.
-- @return The animation list.
--
function Animations.list(group)
  if group then
    return Animations.animations[group]
  end

  return Animations.animations
end

ss.Utils.addModule("Animations", Animations)