## Title: Seamore Spheres
## Version: v-1.0.11-classic
## Author: Seam0re
## Interface: 11305
## Notes: Player Unit Frame Enhancement for World of Warcraft
## SavedVariables: Seamore
## SavedVariablesPerCharacter: Spheres

init.lua
libraries\load_libraries.xml
developer\load_developer.xml
media\load_media.xml
core\load_core.xml
settings\load_settings.xml
modules\load_modules.xml
