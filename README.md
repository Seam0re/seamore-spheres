# Seamore Sheres #



> #### Description ####
> >
> > _Seamore Spheres_ is a _World of Warcraft_ addon providing animated spheres for various unit/event tracking.
> >
> > Includes a predefined list of modes for each class and specialization.
> >
> > _Version 1.0.1_
>
> #### Installation ####
> >
> > This addon must be manually added to _World of Warcraft_'s addon directory. (Default `C:\Program Files (x86)\World of Warcraft\Interface\AddOns`)
> >
> > Upon logging in, only the basic player units are tracked. (Health and main resource respectively.)
>
> #### Configuration ####
> >
> > Artwork textures can be toggled on or off through the config panel.
> >
> > Pet and "secondary" resource spheres can be toggled on or off through the config panel.
> >
> > Holding the `Shift` key will allow sphere groups to be dragged.
> >
> > The scale of the spheres can be adjusted through the "Preview" tab of the config panel.
> >
> > Sphere textures and animations may be modified through the "Preview" tab of the config panel. (No changes are committed until saved)
> > >
> > > Selecting a sphere from the preview tab will display a panel where the sphere's settings may be safely modified.
> > >
> > > The following customizations are available for modes associated with the player's current specialization:
> > >    * Background texture. (Unavailable for multi resource spheres.)
> > >    * Background color and opacity.
> > >    * Rotation texture.
> > >    * Rotation texture color and opacity.
> > >    * Font color and visibility.
> > >    * Text display format.
> > >    * Animation position, opacity, scale and strata.
> > >    * Frame scale and position.
> >
> > Micro spheres may be added to monitor spell charges or buffs/debuffs, these are displayed along the outer edge of a sphere.
> > >
> > > Selecting a micro sphere or secondary micro sphere group from the config panel "Micro Spheres" tab will allow you to edit micro spheres.
> > >
> > > Micro spheres are displayed on the health sphere, group 1 beginning from the top left, and group 2 from the bottom right.
> > >
> > > Secondary micro spheres are displayed on the main resource sphere, group 1 beginning from the top right, and group 2 from the bottom left.
> > >
> > > Additional spell charges, buffs or debuffs may be created by selecting a resource type and choosing "-- Custom --" from the "Add Resource" menu.
> > >
> > > To display separate micro spheres for individual buff/debuff stacks, add each of the matching resources. ("SPELL", "SPELL#" etc. where # is associated with the specific stack amount)
> > >
> > > The list of added resources will be displayed matching the order in which the micro spheres will appear.
> >
> > The following addons provide additional background textures:
> >    * _Seamore Spheres Glass Textures_
> >    * _Seamore Spheres Plastic Textures_
> >    * _Seamore Spheres Sky Textures_
> >    * _Seamore Spheres Technology Textures_
> >
> #### Contribution ####
> >
> > __Testing__
> > >
> > > The more the merrier, This is my first LUA project.
> > >
> > > If you're interested in doing some pre-release testing or would like to test a specific feature, let me know and we can coordinate a stable release branch to match your requirements.
> > >
> > > Feel free to create yourself a testing branch using the following format: `PREFIX--MYUSERNAME--feature_name`
> > > >
> > > > ###### Please use the following prefixes when creating test branches:
> > > >
> > > > * `SSFT` - Feature testing
> > > > * `SSLT` - Logical Testing
> > > > * `SSTC` - Test Cases
> > > > * `SSUT` - Unit Testing
> > > > * `SSUX` - User Experience Testing
> >
> > __Code review__
> > >
> > >  Any and all review is welcome.
> > >
> > >  Please insert code specific comments directly with the related code.
> > >
> > >  Repository comments should be treated as they would in a codebase, please avoid code scatter. (Point with one hand, not both ;D)
> > >
> > >  Comments should be kept in release, release candidate or release test branches to avoid being overseen whenever possible.
> >
> > __Non technical users__
> > >
> > > Have an idea but can't help out? too late you already did!
> > >
> > > Let me hear what you think, I'm doing this for experimental and entertainement purposes and am not very creative... I just want to build (break) things!
>
> #### Resources ####
> >
> > [Mistra's Diablo Orbs]
> >
> > [RothUI Diablo]
> >
> > [Original Repository]
> >
> > [Programming in Lua]
> >
> > [World of Warcraft Programming]
> >
> > [World of Warcraft API]
> >
> > [WoW Event API]
> >
> > [Class Colors]
> >
> > [Resource Units]
> >
> > [Power Colors]
> >
>
> #### Contact Information ####
> >
> > __Seamore Jenkins__, the French Canadian!

[Mistra's Diablo Orbs]: https://mods.curse.com/addons/wow/mistras-diablo-orbs "https://mods.curse.com/addons/wow/mistras-diablo-orbs"
[RothUI Diablo]: http://www.wowinterface.com/downloads/info9175-RothUIDiablo.html "http://www.wowinterface.com/downloads/info9175-RothUIDiablo.html"
[Original Repository]: https://github.com/seam0re/SeamoreSpheres "github.com"
[Programming in Lua]: https://www.lua.org/pil/contents.html "www.lua.org"
[World of Warcraft Programming]: http://wowprogramming.com/ "wowprogramming.com"
[Class Colors]: http://wowwiki.wikia.com/wiki/Class_colors "wowwiki.wikia.com"
[WoW Event API]: http://wowwiki.wikia.com/wiki/Events_A-Z_(Full_List) "wowwiki.wikia.com"
[World of Warcraft API]: http://wowwiki.wikia.com/wiki/World_of_Warcraft_API "wowwiki.wikia.com"
[Resource Units]: http://wow.gamepedia.com/PowerType "wow.gamepedia.com"
[Power Colors]: http://wow.gamepedia.com/Power_colors "wow.gamepedia.com"
[Saved Variables]: https://www.mediafire.com/?309bbfpkn9cphim "www.mediafire.com"